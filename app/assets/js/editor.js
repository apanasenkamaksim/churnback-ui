$(document).ready(function(){

  var methods = {
    init : function() {
      var editor=$(this);
      var tools=editor.find('.tools').first();
      var textarea=editor.find('textarea').first();

      // textarea.css({'padding-top': tools.height()+'px'});
      // editor.resize(function(){
      //   textarea.css({'padding-top': tools.height()+'px'});
      // });

    }
  };

  $.fn.editor = function( method ) {

    return this.each(function(){
      // логика вызова метода
      if ( methods[method] ) {
        return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
      }else if( typeof method === 'object' || ! method ) {
        return methods.init.apply( this, arguments );
      }else{
        $.error( 'Метод с именем ' +  method + ' не существует для jQuery.editor' );
      } 
    });

  }
});