jQuery(document).ready(function($) {

  $(window).on("scroll", function(){Scripts.scroll();});
  $(window).on("resize", function(){Scripts.resize();});

  var Scripts = {
    'init' : function(){
      // Copy To Clipboard
      $('.copyToClipboard').click(function(){
        input=$('#'+$(this).data('id'));
        if(input.length==1){
          input.select();
          document.execCommand("copy");
        }
      });

      // Drag-N-Drop Upload
      $('.drag-n-drop img').attr('data-src',$('.drag-n-drop img').attr('src'));
      $('.drag-n-drop img').attr('data-srcset',$('.drag-n-drop img').attr('srcset'));

      $('.file-upload .title').click(function(){
        $('.drag-n-drop input[type="file"]').trigger('click');
      });
      $('.drag-n-drop input[type="file"]').change(function(){
        dragndrop=$(this).closest('.drag-n-drop');
        $('.file-upload .name').val($(this).val().split('\\').pop());
        if (this.files&&this.files[0]){
            var reader = new FileReader();

            reader.onload=function(e){
              dragndrop.find('img').first().attr('src',e.target.result);
              dragndrop.find('img').first().removeAttr('srcset');
            }

            reader.readAsDataURL(this.files[0]);
        }else{
          $('.drag-n-drop img').attr('src',$('.drag-n-drop img').attr('data-src'));
          $('.drag-n-drop img').attr('srcset',$('.drag-n-drop img').attr('data-srcset'));
        }
      });

      // Upload btn

      $('.upload_btn input[type="file"]').change(function(){
        upload_btn=$(this).closest('.upload_btn');
        upload_btn.find('.file_name').first().text($(this).val().split('\\').pop());
      });

      // Multiselect
      if($.isFunction($.fn.multiselect)){
        $('.multiselect').multiselect('init');
      }

      // Editor
      if($.isFunction($.fn.editor)){
        $('.editor').editor('init');
      }

      // Timeline tabs
      if($.isFunction($.fn.timeline_tabs)){
        $('.tabs').timeline_tabs('init');
      }

      // Full-height page-wrapper
      if($(window).width() >= 992){
        $('.page-wrapper.full-height').height($(window).innerHeight());
      }else{
        $('.page-wrapper.full-height').css({height: 'auto'});
      }
    },
    'scroll' : function(){

    },
    'resize' : function(){
      // Full-height page-wrapper
      if($(window).width() >= 992){
        $('.page-wrapper.full-height').height($(window).innerHeight());
      }else{
        $('.page-wrapper.full-height').css({height: 'auto'});
      }
    }
  };

  Scripts.init();


});
